# Spacestagram

Fetches the most recent NASA [Astronomy Pictures of the Day](https://apod.nasa.gov/apod/astropix.html) and displays them with like/unlike functionality.

![Spacestagram Demo](https://gitlab.com/elskhn/spacestagram/-/raw/master/spacestagram.png)

Supports dekstop, mobile, and light/dark mode.

### Run
To run this project locally, perform these commands:
```
yarn install

yarn dev
```

And visit [http://localhost:3000](http://localhost:3000)
