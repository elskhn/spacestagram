module.exports = {
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/line-clamp")],
  content: ["./app/**/*.{js,jsx}", "./app/*.{js,jsx}"],
};
