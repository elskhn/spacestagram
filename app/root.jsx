import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration
} from "remix";

import tailwindStyles from "./tailwind.css"
import rootStyles from "./index.css"

export function meta() {
  return { title: "Spacestagram", };
}

export function links() {
  return [
    {
      rel: "stylesheet",
      href: tailwindStyles
    },
    {
      rel: "stylesheet",
      href: rootStyles
    },
  ];
}


export default function App() {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />

      </head>
      <body className="bg-slate-100  dark:bg-[#0c2233] transition-all overflow-visible">
        {/* <body className="bg-slate-100  dark:bg-slate-900  transition-all overflow-visible"> */}
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <script src="/lib/cosha.min.js"></script>
        {process.env.NODE_ENV === "development" && <LiveReload />}
      </body>
    </html >
  );
}
