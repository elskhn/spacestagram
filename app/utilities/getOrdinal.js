export default function getOrdinal(n) {
  let ord = ["st", "nd", "rd"];
  let exceptions = [11, 12, 13];
  let nth =
    ord[(n % 10) - 1] == undefined || exceptions.includes(n % 100)
      ? "th"
      : ord[(n % 10) - 1];
  return nth;
}
