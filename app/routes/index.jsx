import { useLoaderData } from "remix";

import dayjs from "dayjs";
import ImgPost from "~/components/ImgPost";
import { useEffect } from "react";

export async function loader() {
  let res = await fetch(
    `https://api.nasa.gov/planetary/apod?api_key=${process.env.NASA_API_KEY}&end_date=${dayjs().format("YYYY-MM-DD")}&start_date=${dayjs().subtract(5, "day").format("YYYY-MM-DD")}`
  );
  return res.json();
}

export default function Index() {
  useEffect(() => {
    if (typeof cosha == "function") {
      cosha("img-shadow", { blur: "10px", y: "8px" })

      document.querySelectorAll(".img-shadow-clone").forEach(el => {
        el.setAttribute("role", "presentation");
      })
    }
  }, [])


  let data = useLoaderData(loader);

  return (
    <main className="m-auto py-3 pt-9 md:pt-5 md:p-8 px-4 md:px-6 transition-all max-w-3xl">
      <header className="max-w-2xl m-auto px-3 md:px-0">
        <h1 className="text-3xl text-gray-700 mb-1 md:mb-2 font-extrabold dark:text-white md:text-4xl transition-all">
          Spacestagram
        </h1>
        <h2 className="text-sm mb-5 font-medium text-slate-500 dark:text-gray-300">
          NASA's Images of the Day
        </h2>
      </header>

      {data &&
        data.reverse().map((item, index) => (
          <ImgPost {...item} key={index} />
        ))
      }
    </main>
  );
}
