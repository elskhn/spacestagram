import dayjs from "dayjs";
import { Heart } from "react-feather";
import getOrdinal from "~/utilities/getOrdinal";

import AnimatedShowMore from 'react-animated-show-more';
import { useEffect, useState } from "react";

export default function ImgPost({ url: imgSrc, title, copyright, explanation, date }) {

  let dayOrdinal = getOrdinal(new Date().getDate())

  let dateString = `${dayjs(date).format("D")}${dayOrdinal} ${dayjs(date).format("MMMM")}`

  const [isLiked, setIsLiked] = useState(false)

  let toggleLike = (date) => {
    let existing = localStorage.getItem('likedPosts');
    existing = existing ? existing.split(',') : [];
    if (existing.includes(date)) {
      existing = existing.filter(item => item !== date);
      setIsLiked(false)
    } else {
      existing.push(date);
      setIsLiked(true)
    }
    localStorage.setItem('likedPosts', existing.toString());
  }

  useEffect(() => {
    let existing = localStorage.getItem('likedPosts');
    existing = existing ? existing.split(',') : [];

    if (existing.includes(date)) {
      setIsLiked(true)
    }
  }, [])

  return (
    <section className="w-100 bg-white dark:bg-[#132c40] rounded-2xl p-3 transition-all max-w-3xl shadow-xl shadow-slate-200 dark:shadow-none relative mb-5">
      <img
        height={416}
        width={688}
        className="w-full h-auto rounded-xl mb-2 img-shadow z-10 lg:min-h-[400px] min-h-auto"
        src={imgSrc}
        alt={`Image: ${title}`}
      />

      <div className="p-2">
        <h2 className="text-xl font-bold dark:text-slate-100 text-gray-600 mt-0">
          {title}
        </h2>
        <p className="text-xs text-slate-400 dark:text-slate-400 mb-2 font-semibold">
          {copyright ?
            `${copyright} – ` : ""
          }
          <time dateTime={dayjs(date).toISOString()}>{dateString}</time>
        </p >
        <AnimatedShowMore
          height={50}
          toggle={({ isOpen }) => isOpen ?
            <p className="float-right text-sm text-blue-600 dark:text-blue-500">...show less</p> :
            <p className="float-right text-sm text-blue-600 dark:text-blue-500">show more...</p>
          }
          speed={200}
        // shadowColor="#ffffff">
        >
          <span className="text-sm text-slate-600 dark:text-slate-300 text-justify">
            {explanation}
          </span>
        </AnimatedShowMore>
      </div >

      {
        isLiked ?
          <button className="relative text-sm border-slate-100 font-medium bg-rose-100 text-rose-400 focus:text-rose-400 transition-[100ms] cursor-pointer flex items-center justify-center whitespace-nowrap pl-4 pr-5 min-h-[32px] m-2 rounded-lg focus:ring-rose-200 focus:ring focus:outline-none dark:bg-[#19364d] dark:hover:bg-[#19364d] dark:focus:ring-rose-400 mt-3" onClick={() => toggleLike(date)}>
            <Heart className="mr-2 anim-scale-in-center" size={14} strokeWidth={2.5} fill="rgb(251, 115, 134)" />
            Unlike
          </button>
          :

          <button className="relative text-sm bg-slate-100 border-slate-100 text-slate-500 font-medium hover:bg-rose-100 hover:text-rose-400 focus:text-rose-400 transition-[100ms] cursor-pointer flex items-center justify-center whitespace-nowrap pl-4 pr-5 min-h-[32px] m-2 rounded-lg focus:ring-rose-200 focus:ring focus:outline-none dark:bg-[#19364d] dark:hover:bg-[#19364d] dark:focus:ring-rose-400 dark:text-slate-300 mt-3" onClick={() => toggleLike(date)}>
            <Heart className="mr-2" size={14} strokeWidth={2.5} />
            Like
          </button>}
    </section >
  )
}
